<!DOCTYPE html>
<html dir="ltr" lang="ru">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Места — сервис просмотра и редактирования библиотек геометок</title>
	<link href="/css/style.css" rel="stylesheet" type="text/css">
	<link href="/css/layout.css" rel="stylesheet" type="text/css">
	<link href="/css/grid.css" rel="stylesheet" type="text/css">

	<script src="/vendor/scripts/vue.js"></script>
	<script src="/vendor/scripts/vuex"></script>
<!--
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://unpkg.com/vuex"></script>
-->
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru-RU"></script>
	<script src="/vendor/scripts/css-element-queries/src/ResizeSensor.js"></script>
	<script src="/scripts/fields_validate.js"></script>
	<script src="/scripts/randomstring.js"></script>
	<script src="/scripts/account.js"></script>
<!--
	<script src="/vendor/scripts/css-element-queries/src/ElementQueries.js"></script>
-->
</head>
<body>
	<div id="app"></div>
	<script src="/dist/build.js"></script>
</body>
</html>
